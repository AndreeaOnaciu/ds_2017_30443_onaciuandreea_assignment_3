package consumer;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.concurrent.TimeoutException;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Consumer;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;

public class DVDPrintConsumer {
	  private static final String EXCHANGE_NAME = "logs";
	  private static final String path="D:\\Facultate\\An4\\DistributedSystems\\Assignment3.2\\assignment3.2\\assignment3.2\\the-file-name.txt";
	  Channel channel;
	  Connection connection;
	  
	  public  void consume() throws IOException, TimeoutException{
	   
		ConnectionFactory factory = new ConnectionFactory();
	    factory.setHost("localhost");
	    connection = factory.newConnection();
	    channel = connection.createChannel();

	    channel.exchangeDeclare(EXCHANGE_NAME, BuiltinExchangeType.FANOUT);
	    String queueName = channel.queueDeclare().getQueue();
	    channel.queueBind(queueName, EXCHANGE_NAME, "");
	    
	    System.out.println(" [*] Waiting for messages. To exit press CTRL+C");

	    Consumer consumer = new DefaultConsumer(channel) {
	      @Override
	      public void handleDelivery(String consumerTag, Envelope envelope,
	                                 AMQP.BasicProperties properties, byte[] body) throws IOException {
	        String message = new String(body, "UTF-8");
	        Files.write(Paths.get(path), (message+"\n").getBytes(), StandardOpenOption.APPEND);
	        
	        System.out.println(" [x] Received '" + message + "'");
	        
	       /* try {
				channel.close();
			} catch (TimeoutException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	        connection.close();*/
	      }
	    };
	    channel.basicConsume(queueName, true, consumer);
	    
	  }
	  
	  public void stop() throws IOException, TimeoutException{
		channel.close();
		connection.close();
	  }
}