package servlet;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import consumer.DVDEmailConsumer;
import consumer.DVDPrintConsumer;
import entities.DVD;
import producer.DVDProducer;


public class DVDServlet extends HttpServlet {

	DVDProducer dvdProducer=new DVDProducer();
	DVDEmailConsumer dvdEmailConsumer=new DVDEmailConsumer();
	DVDPrintConsumer dvdPrintConsumer=new DVDPrintConsumer();
	
	public DVDServlet() {
		try {
			dvdEmailConsumer.consume();
			dvdPrintConsumer.consume();
		} catch (IOException | TimeoutException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	
	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		response.sendRedirect("index.html");
		
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String title= request.getParameter("title");
		String price = request.getParameter("price");
		String year = request.getParameter("year");

		DVD dvd=new DVD(title,Integer.valueOf(year),Double.valueOf(price));

		dvdProducer.produce(dvd.toString());
		response.sendRedirect("index.html");
	}
	
}
